<?php
declare(strict_types=1);
	
	/**
	 * User class
	 */
	
	class User {
		private $name;
		private $year;
        private $email;
        private $pw;
		private	$gender;
		private $lang;
        private $class1;
        private $class2;
        private $numproj;
        private $image; 
        //$_FILES['propic']['name'];
	
		public function __construct(string $pname, int $yr, string $em, string $pass, string $gen, string $lan, string $c1, string $c2, int $num, string $img) {
			$this->name = $pname;		
			$this->year = $yr; 
            $this->email = $em;
            $this->pw = $pass;
            $this->gender = $gen;
            $this->lang = $lan;
            $this->class1 = $c1;
            $this->class2 = $c2;
            $this->numproj = $num;
            $this->image = $img;
            
		}
	
		public function __toString() {
			return "<b>Username:</b> ".$this->name.", <b>Email:</b> ".$this->email;		
		}

		public function getName() : string {
			return $this->name;
		}
	
		public function getEmail() : string {
			return $this->email;
		}
        public function getPassword() : string {
			return $this->pw;
		}
        public function getYear() : int {
			return $this->year;
		}
        public function getGender() : string {
			return $this->gender;
		}
        public function getLanguage() : string {
			return $this->lang;
		}
        public function getClass1() : string {
			return $this->email;
		}
        public function getClass2() : string {
			return $this->email;
		}
        public function getNumProject() : int {
			return $this->numproj;
		}
        public function getProfilePic() : string {
			return $this->image;
		}
        
        function __set($name,$value){
            if(method_exists($this, $name)){
              $this->$name($value);
            }
            else{
              // Getter/Setter not defined so set as property of object
              $this->$name = $value;
            }
        }
	
		public function __destruct() {
			echo "Person __destruct called<br />";
		}
		
		public final function getCode() {
			return 09876;
		}
		
	}
?>