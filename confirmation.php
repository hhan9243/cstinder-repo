<html>
    <head>
	<link rel="stylesheet" href="group_style.css"/>
    </head>

    <body>

	<?php
    
        require_once("support.php");
		require_once ("dbkeys.php");
		
		$title = "Confirmation";
		
		$db = new mysqli($host, $user, $password, $database);
		if ($db->connect_error) {
			die($db->connect_error);
		}
        
		$name = $_POST['name'];
		$year = $_POST['year'];
        $year = intval($year);
		$pw = $_POST['pw'];
		$gender = $_POST['gender'];
		$email = $_POST['email'];
        $lang = $_POST['lang'];
		$class_arr = $_POST['classes'];
        $class = implode(" ",$_POST['classes']);
        $numproj = $_POST['numProj'];
        $numproj = intval($numproj);
        
    	$encpw = password_hash($pw, PASSWORD_BCRYPT);
		
		$class1 = $class_arr[0];
		$class2 = $class_arr[1];
		
		
		/* Query */
		$sqlQuery = "insert into $table (name, year, password, gender, email, language, class1, class2, numProject)
					VALUES ('$name', '$year', '$encpw', '$gender', '$email', '$lang', '$class1', '$class2', '$numproj')";
    
        /* Executing query */
		$result = $db->query($sqlQuery);
		if (!$result) {
			die("Insertion failed: " . $db->error);
		}
    
 		/* Closing connection */
		$db->close();
        
        
		$body =<<< EBODY
			<h1>The following entry has been added to our database!</h1>
			<strong>Name:</strong> $name<br/>
			<strong>Email:</strong> $year<br/>
			<strong>Gender:</strong> $gender</br>
			<strong>Email:</strong> $email</br>
			<strong>Favorite Language:</strong> $lang<br/>
            <strong>Class1:</strong> $class1<br/>
			<strong>Class2:</strong> $class2<br/>
            <strong>Number of Projects Completed:</strong> $numproj<br/>
			</br>
			<form action = "main.html">
			<input type ="submit" value = "Return to main menu"/>
			</form>
EBODY;
        
    
    
    		 echo generatePage($body, $title);
	?>
    
	
	<script>
		main();
		
		function main(){
		var arr = document.getElementById("classes").val();
		print(typeof(arr));
		if(arr.length > 2){
			window.alert("Select up to 2 classes.");
		    return false;
			}
		}
	</script>
	
	
	
	
    </body>

</html>