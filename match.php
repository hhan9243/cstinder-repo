<?php
require_once("support.php");
require_once("dbkeys.php");

session_start();
$title = "Search";

if(isset($_POST['keep'])){
	header("Location: search.php");
}
if(isset($_POST['talk'])){
	header("Location: profile.php"); //confirmation page for this?
}
$match = <<<EOBODY
<img id="stars" src="star_background" alt="match_bg">
<h1 id="match_id">It's a match!</h2>
<form action="" method="post">
<img src="glasses.png" alt="glasses">
<img id="match_1" src=$match_1 alt="match1">
<!--This is the current user's profile pic-->
<img id="match_2" src=$_SESSION['match'][$_SESSION['email']] alt="match2">

<img src="speech_bubble.png" alt="speech_bubble">
<img src="gear.pngsli" alt="gear">
<input type="submit" name="talk" value="Talk Details">
<input type="submit" name="keep" value="Keep Searching">
</form>
EOBODY;

echo generatePage($body, $title);
?>