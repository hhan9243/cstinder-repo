<!doctype html>
<html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="PPDesign.css"/>
    <title>Profile Page</title>   

    </head>
    <body onload = "main()">
        <div class="row" style="padding:2em">
            <div class="col-md-7" style="height:90vh;border-style:solid; border-color:gray">
                <div class="row" style="height:55%;background-color:gold;text-align:center;padding:0.4em">
                    <div class="col-sm-2">
    				</div>
                    <div class="col-sm-8" style="height:100%">
                    	<img id="image-cropper" src="profile_demo.png" style='height: 100%; object-fit: contain'/>
                    </div>
                </div>
                <div class="row" style="height:5%;background-color:gold;text-align:center">
              		<div id="nameSpace" class="col-sm-6">
              		</div>
              		<div id="yearSpace" class="col-sm-6">
              		</div>
                </div>
                <div class="row" style="height:15%;background-color:darkgray;padding:1em">
                    <div id="coding" class="col-md-4" style="height:100%; text-align: center">
                        <img src="coding.png" style='height: 100%; width: 100%; object-fit: contain'/>
                    </div>
                    <div id="email" class="col-md-4" style="height:100%; text-align: center">
                        <img src="mail.png" style='height: 100%; width: 100%; object-fit: contain'/>
                    </div>
            		<div id="proj" class="col-md-4" style="height:100%; text-align: center">
                    	<img src="folder.png" style='height: 100%; width: 100%; object-fit: contain'/>
            		</div>
                </div>
                <div class="row" style="height:25%;background-color:lightgray;padding:1em">
            		<div class="col-md-4" style="height:100%">
                    	<a href="mainPage.php">
                    	<img src="logout.png" style='height: 100%; width: 100%; object-fit: contain'/>
                    	</a>            
            		</div>
    				<div class="col-md-4" style="height:100%">
                    	<a href="createAccount.php">
                    	<img src="wrench.png" style='height: 100%; width: 100%; object-fit: contain'/>
                    	</a>            
            		</div>
            		<div class="col-md-4" style="height:100%">
                    	<a href="search.php">
                    	<img src="search.png" style='height: 100%; width: 100%; object-fit: contain'/>
                    	</a>            
            		</div>
                </div>
            </div>
            <div id="matchesContentDiv" class="col-md-5" style="height:90vh;border-style:solid; border-color:white; background-color:white">
            <h4 id="matchesContent"></h4>
            </div>
         </div>
<?php
    require_once("support2.php");
    require_once("dbkeys.php");
    session_start();

    $db = new mysqli($host, $user, $password, $database);
    if ($db->connect_error) {
        die($db->connect_error);
    }
    if (isset($_SESSION['email'])) {
        $email = $_SESSION['email'];
        $sqlQuery = "SELECT name,image,year,language,numProject FROM `cstable` WHERE email = \"$email\"";

        $result = $db->query($sqlQuery);
        if (!$result) {
            die($db->error);
        }
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $name = $row['name'];
        $image = $row['image'];
        $year = $row['year'];
        $language = $row['language'];
        $numProject = $row['numProject'];
    } else {
        header("Location: mainPage.php");
    }
?>

    <script type="text/javascript">
    "use strict";

    function main() {
        document.getElementById("image-cropper").src = "<?php echo $image?>";
        document.getElementById("nameSpace").innerHTML = "Name: " + "<?php echo $name?>";
        document.getElementById("yearSpace").innerHTML = "Year: " + "<?php echo $year?>";

        let backBut = document.getElementById("back");

        let projIcon = document.getElementById("proj");

        projIcon.onmouseout = function() {
            document.getElementById('proj').innerHTML = '<img src=\"folder.png\" style=\"height: 100%; width: 100%; object-fit: contain; text-align:center\">';
        }

        projIcon.onmouseover = function() {
            document.getElementById('proj').innerHTML = "Number of <br>Projects Done:<br>" + "<?php echo $numProject?>";
        }


        let codIcon = document.getElementById("coding");


        codIcon.onmouseout = function() {
            document.getElementById('coding').innerHTML = '<img src=\"coding.png\" style=\"height: 100%; width: 100%; object-fit: contain; text-align:center\">';
        }

        codIcon.onmouseover = function() {
            document.getElementById('coding').innerHTML = "Favorite Language:<br>" + "<?php echo $language?>";
        }

        let emailIcon = document.getElementById("email");


        emailIcon.onmouseout = function() {
            document.getElementById('email').innerHTML = '<img src=\"mail.png\" style=\"height: 100%; width: 100%; object-fit: contain; text-align:center\">';
        }

        emailIcon.onmouseover = function() {
            document.getElementById('email').innerHTML = "Your Email:<br>" + "<?php echo $email?>";
        }

    }
    </script>
    </body>
</html>