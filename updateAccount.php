<html>
    <head>
        <link rel="stylesheet" href="group_style.css"/>
        <script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script type="text/javascript" src = "./javascript.js"></script>
    </head>
    
    <body onload="main()">
    <?php
        
        require_once("support.php");
        require_once("dbkeys.php");
        
        session_start();
            
        $title = "Submit Application";
        
    
        if(!isset($_POST['submitUpdate'])){
           //if(isset($_SESSION['email'])){
                $current = $_SESSION['email'];
                $m_flag = "";
                $f_flag = "";
                $e_flag = "";
                    
                $sqlQuery = "SELECT * FROM `cstable` WHERE email = \"$current\"";
                $db = new mysqli($host, $user, $password, $database);
                $result = $db->query($sqlQuery);
                if (!$result) {
                    die($db->error);
                }
					$row = $result->fetch_array(MYSQLI_ASSOC);
					$name = $row['name'];
					$image = $row['image'];
					$pw = $row['password'];
					$year = $row['year'];
					$gender = $row['gender'];
					$language = $row['language'];
					$numProj = intval($row['numProject']);

                    
                if($row['gender'] == "M"){
                    $m_flag = "checked";
                }
                else if ($row['gender'] == "F"){
                    $f_flag  = "checked";
                }
                else if($row['gender'] == "else"){
                    $e_flag  = "checked";
                }
				
				
            //}
                    
                $body = <<<EOBODY
                <form id="form_container" action = "{$_SERVER["PHP_SELF"]}" method = "post" enctype = "multipart/form-data">
                <div>
                
                Upload your profile picture: <input type = "file" name = "propic" id="fileElement" value = "$image" onchange = "display(this);" required>
                <img id = "displayArea" src = "$image"/>
                </div>
                </br>
                <fieldset id="info_container">
                    <strong>Name: </strong><input type = "text" id="name" name = "name" class = "text" value = "$name" required></br></br>
                    <strong>Year: </strong><input type = "text" id="year" name = "year" class = "text" value = "$year" placeholder = "Ex: 12" required></br></br>
                    <strong>Password: </strong><input type = "password" id="pw" name = "pw" class = "text" required></br></br>
                    <strong>Gender: </strong><input type = "radio" id="gender" name = "gender" value = "M" $m_flag />M
                             <input type = "radio" id="gender" name = "gender" value = "F" $f_flag/>F
                             <input type = "radio" id="gender" name = "gender" value = "else" $e_flag/>Non-Binary
                             </br></br>
                    <strong>Email: </strong><input type = "email" name = "email" class = "text" value="$current" required/></br></br>
                    <strong>Favorite Language: </strong><input type = "text" name = "lang" class = "text" value = "$language" required></br></br>
                    <strong>Current Classes (Up to 2): </strong><select id="classes" name = "classes[]" multiple required>
                                                <option value = "CMSC122">CMSC122</option>
                                                <option value = "CMSC131">CMSC131</option>
                                                <option value = "CMSC132">CMSC132</option>
                                                <option value = "CMSC216">CMSC216</option>
                                                <option value = "CMSC250">CMSC250</option>
                                                <option value = "CMSC330">CSMC330</option>
                                                <option value = "CMSC351">CMSC351</option>
                                                <option value = "CMSC389N">CMSC389N</option>
                                                <option value = "CMSC411">CMSC411</option>
                                                <option value = "CMSC412">CMSC412</option>
                                                <option value = "CMSC414">CMSC414</option>
                                                <option value = "CMSC417">CMSC417</option>
                                                <option value = "CMSC420">CMSC420</option>
                                                <option value = "CMSC421">CMSC421</option>
                                                <option value = "CMSC422">CMSC422</option>
                                                <option value = "CMSC423">CMSC423</option>
                                                <option value = "CMSC424">CMSC424</option>
                                                <option value = "CMSC426">CMSC426</option>
                                                <option value = "CMSC427">CMSC427</option>
                                                <option value = "CMSC430">CMSC430</option>
                                                <option value = "CMSC433">CMSC433</option>
                                                <option value = "CMSC434">CMSC434</option>
                                                <option value = "CMSC435">CMSC435</option>
                                                <option value = "CMSC436">CMSC436</option>
                                                <option value = "CMSC451">CMSC451</option>
                                                <option value = "CMSC460">CMSC460</option>
                                                <option value = "CMSC466">CMSC466</option>
                                                <option value = "CMSC474">CMSC474</option>
                                                <option value = "other">Other</option>
                                           </select>
                    </br></br>
                    <strong>Number of Projects Completed:</strong></br>
                    <input id="range_num" type = "range" name = "numProj" max = "40" min = "0"/>
                </fieldset>
                </br>
                <input type = "submit" name = "submitUpdate" value = "SUBMIT"/>
                </form>
                
                <form action = "mainPage.php" method = "post">
                <input type = "submit" value = "BACK"/>
                </form>
EOBODY;
                echo generatePage($body, $title);

                
            }
            else { 
                    $db = new mysqli($host, $user, $password, $database);
                    if ($db->connect_error) {
                        die($db->connect_error);
                    }
                    
                    $name = $_POST['name'];
                    $year = $_POST['year'];
                    $year = intval($year);
                    $pw = $_POST['pw'];
                    $gender = $_POST['gender'];
                    $newEmail = $_POST['email'];
                    $lang = $_POST['lang'];
                    $class_arr = $_POST['classes'];

                    $class = implode(" ",$_POST['classes']);
                    $numproj = $_POST['numProj'];
                    $numproj = intval($numproj);
                    $profile  = basename($_FILES['propic']['name']);
                    
                    $encpw = password_hash($pw, PASSWORD_BCRYPT);
                    
                    $class1 = $class_arr[0];
                    $class2 = $class_arr[1];
                    
                    $image = $_FILES['propic']['name'];

                    /* Query */
                    
                    //if (!isset($_SESSION['email'])){
                    //    $sqlQuery = "insert into $table (name, year, password, gender, email, language, class1, class2, numProject, image)
                    //            VALUES ('$name', '$year', '$encpw', '$gender', '$newEmail', '$lang', '$class1', '$class2', '$numproj', '$image')";
                    //}
                    //else {
                        $current = $_SESSION['email'];
                        $sqlQuery = "update $table set name = '$name',
                                                email = '$newEmail',
                                                year = '$year',
                                                gender = '$gender',
                                                password = '$encpw',
                                                image = '$image',
                                                class1 = '$class1',
                                                class2 = '$class2',
                                                numProject = '$numProj'
                                                where email = '$current'";    
                    //}
                    $_SESSION['email'] = $newEmail;

                    move_uploaded_file($_FILES['propic']['tmp_name'], $profile);
                    
                
                    /* Executing query */
                    $result = $db->query($sqlQuery);
                    if (!$result) {
                        die("Insertion failed: " . $db->error);
                    }
                
                    /* Closing connection */
                    $db->close();
                    
                    
                    header("location: profilePage.php");
            
            }
            
            ?>
    <script>
            "use strict";
            
            function main(){
                var year = document.getElementById("year").value;
                if(isNaN(year)){
                    alert("Please enter a valid number for Year section.");
                }
            }
            
            function display(input) {
                var reader = new FileReader();
                
                reader.onload = function(event) {
                    $('#displayArea').attr("src",event.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            
            }
          
        </script>
    </body>
</html>
